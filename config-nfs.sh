#!/bin/bash

# Variable declarations
export_dir="/var/export/testvol"
export_file="/etc/exports.d/testvol.exports"
master_hostname="services"

  if [[ $(hostname -s) != ${master_hostname} && ${UID} -ne "0" ]]; then
    echo "This script must be run on the ${master_hostname} host as root."
    exit 1
  fi

  if [ -d ${export_dir} ]; then
    echo "Export directory ${export_dir} already exists."
  else
    mkdir -p ${export_dir}
    chown nfsnobody:nfsnobody /var/export/testvol
    chmod 700 /var/export/testvol
    echo "Export directory ${export_dir} created."
  fi

  if [ -f ${export_file} ]; then
    echo "Export file ${export_file} already exists."
  else
    echo "/var/export/testvol *(rw,async,all_squash)" > /etc/exports.d/testvol.exports
    exportfs -a
    showmount -e
  fi
